#!/bin/bash
err() {
    echo "FATAL ERROR: ${1}"
    [[ -z ${2} ]] && cleanFail

    exit 1
}

passwd() {
    openssl rand -base64 "${1}" | head -c "${1}"
}

cleanFail() {
    echo "$(tput bold)[$(date -u +"%Y-%m-%dT%H:%M:%SZ")]$(tput setaf 1)[ERROR]$(tput sgr0) Clean installation before exit ...$(tput sgr0)"

    rm /etc/nginx/sites-enabled/*
    rm /etc/nginx/sites-available/*

    sudo -u postgres psql -c "DROP DATABASE ${PEERTUBE_DB};"
    sudo -u postgres psql -c "DROP OWNED BY ${PEERTUBE_USER};"
    sudo -u postgres psql -c "DROP USER ${PEERTUBE_USER};"

    userdel peertube
    rm -rf "${PEERTUBE_FOLDER}"
}

## CHECK
# Check root
if [[ ${EUID} -ne 0 ]]; then
    err "${0##*/} must be run as root"
fi

. Debian10_Install_Peertube20.conf
exec > >(tee -i "${INSTALL_SCRIPT_LOGFILE}")

echo "$(tput bold)[$(date -u +"%Y-%m-%dT%H:%M:%SZ")]$(tput setaf 2)[INFO]$(tput sgr0) Start Peertube installation script ...$(tput sgr0)"

# Uptodate
apt update
yes | apt upgrade
yes | apt install wget curl openssl dnsutils ufw speedtest-cli || err "Fail to run apt install"

# Check DNS
echo "$(tput bold)[$(date -u +"%Y-%m-%dT%H:%M:%SZ")]$(tput setaf 2)[INFO]$(tput sgr0) Check DNS/IP concordance ...$(tput sgr0)"
srv_ip=$(curl ifconfig.me)
add_record=$(dig +short ${SERVEUR_HOST})

if [[ ${add_record} != *${srv_ip}* ]]; then
        err "L'entrée \"${SERVEUR_HOST} IN A·AAA ${srv_ip}\" n'éxiste pas" \
        "et/ou n'est pas joignable"
fi

## DEPENDENCES
echo "$(tput bold)[$(date -u +"%Y-%m-%dT%H:%M:%SZ")]$(tput setaf 2)[INFO]$(tput sgr0) Build/Install dependency ...$(tput sgr0)"
# Install NodeJs 10.x
echo "$(tput bold)[$(date -u +"%Y-%m-%dT%H:%M:%SZ")]$(tput setaf 2)[INFO]$(tput sgr0) Install NodeJS ...$(tput sgr0)"
curl -sL https://deb.nodesource.com/setup_10.x | bash -
yes | apt-get install -y nodejs || err "Fail to install nodejs"

# Add Yarn miror
echo "$(tput bold)[$(date -u +"%Y-%m-%dT%H:%M:%SZ")]$(tput setaf 2)[INFO]$(tput sgr0) Install Yarn ...$(tput sgr0)"
curl -sS https://dl.yarnpkg.com/debian/pubkey.gpg | sudo apt-key add -
echo "deb https://dl.yarnpkg.com/debian/ stable main" | sudo tee /etc/apt/sources.list.d/yarn.list

# Install all requirement
echo "$(tput bold)[$(date -u +"%Y-%m-%dT%H:%M:%SZ")]$(tput setaf 2)[INFO]$(tput sgr0) Install dep from official miror ...$(tput sgr0)"
apt update
yes | apt install unzip yarn sudo unzip vim nginx ffmpeg postgresql postgresql-contrib g++ make redis-server git python-dev || err "Fail to run apt install"

# Check vertions
echo "$(tput bold)[$(date -u +"%Y-%m-%dT%H:%M:%SZ")]$(tput setaf 3)[WARN]$(tput sgr0) ffmpeg version should be >= 3.x$(tput sgr0)"
ffmpeg -version | head -1

echo "$(tput bold)[$(date -u +"%Y-%m-%dT%H:%M:%SZ")]$(tput setaf 3)[WARN]$(tput sgr0) g++ version should be >= 5.x$(tput sgr0)"
g++ -v | tail -1

sleep 15

# Start redis and pgsql
systemctl enable redis postgresql
systemctl start redis postgresql || err "Fail to start redis and pgsql"

## INSTALLATION
echo "$(tput bold)[$(date -u +"%Y-%m-%dT%H:%M:%SZ")]$(tput setaf 2)[INFO]$(tput sgr0) Install PEERTUBE ...$(tput sgr0)"
# Peertube user
useradd -m -d ${PEERTUBE_FOLDER} -s /bin/bash -p ${PEERTUBE_USER} ${PEERTUBE_USER} || err "Fail to create peertube user"

# PGSQL Prepare
PEERTUBE_DB_PASSWORD="$(passwd 12)"

sudo -u postgres psql -c "create user ${PEERTUBE_USER} with password '${PEERTUBE_DB_PASSWORD}';"
sudo -u postgres createdb -O ${PEERTUBE_USER} ${PEERTUBE_DB}
sudo -u postgres psql -c "CREATE EXTENSION pg_trgm;" ${PEERTUBE_DB}
sudo -u postgres psql -c "CREATE EXTENSION unaccent;" ${PEERTUBE_DB}

# Prepare Peertube
PEERTUBE_HTTP_USER="root"
export PT_INITIAL_ROOT_PASSWORD="$(passwd 12)"

VERSION=$(curl -s https://api.github.com/repos/chocobozzz/peertube/releases/latest | grep tag_name | cut -d '"' -f 4)
echo "Latest Peertube version is ${VERSION}"

cd ${PEERTUBE_FOLDER} || err "Can't cd into ${PEERTUBE_FOLDER}"
mkdir config storage versions || err "Can't mkdir on ${PEERTUBE_FOLDER}"

cd versions
wget -q "https://github.com/Chocobozzz/PeerTube/releases/download/${VERSION}/peertube-${VERSION}.zip" || err "Unable to dl peertube"
unzip peertube-${VERSION}.zip || err "Error for unzip peertube"
rm peertube-${VERSION}.zip

# Install Peertube
cd ${PEERTUBE_FOLDER}
chown -R ${PEERTUBE_USER}:${PEERTUBE_USER} ${PEERTUBE_FOLDER}
sudo -u ${PEERTUBE_USER} ln -s versions/peertube-${VERSION} ./peertube-latest

cd ./peertube-latest || err "Error on peertube-latest procedure"
sudo -H -u ${PEERTUBE_USER} yarn install --production --pure-lockfile

# Configure Peertube
cd ${PEERTUBE_FOLDER}

mkdir -p ${PEERTUBE_DATAS}|| err "Can't mkdir ${PEERTUBE_FOLDER}"
chown -R ${PEERTUBE_USER}:${PEERTUBE_USER} ${PEERTUBE_DATAS}

SERVEUR_HARDWARE="
**CPU**
$(lscpu | grep -E "Model name")

**RAM**
Total: $(free -ht | grep "Total" | tr -s " " | cut -f2 -d" ") System Memory

**NetWork**
$(speedtest-cli | grep -E "Download:|Upload:" | sed 's/\/s/\/s    /')
"

sudo -u ${PEERTUBE_USER} cat <<EOF >"${PEERTUBE_CONFIG_FILE}"
listen:
  hostname: 'localhost'
  port: ${PEERTUBE_PORT}

# Correspond to your reverse proxy server_name/listen configuration
webserver:
  https: true
  hostname: '${SERVEUR_HOST}'
  port: 443

rates_limit:
  api:
    # 50 attempts in 10 seconds
    window: 10 seconds
    max: 50
  login:
    # 15 attempts in 5 min
    window: 5 minutes
    max: 15
  signup:
    # 2 attempts in 5 min (only succeeded attempts are taken into account)
    window: 5 minutes
    max: 2
  ask_send_email:
    # 3 attempts in 5 min
    window: 5 minutes
    max: 3

# Proxies to trust to get real client IP
# If you run PeerTube just behind a local proxy (nginx), keep 'loopback'
# If you run PeerTube behind a remote proxy, add the proxy IP address (or subnet)
trust_proxy:
  - 'loopback'

# Your database name will be "peertube"+database.suffix
database:
  hostname: 'localhost'
  port: 5432
  suffix: '${PEERTUBE_DB_SUFFIX}'
  username: '${PEERTUBE_USER}'
  password: '${PEERTUBE_DB_PASSWORD}'
  pool:
    max: 5

# Redis server for short time storage
# You can also specify a 'socket' path to a unix socket but first need to
# comment out hostname and port
redis:
  hostname: 'localhost'
  port: 6379
  auth: null
  db: 0

# SMTP server to send emails
smtp:
  hostname: ${SMTP_HOSTNAME}
  port: ${SMTP_PORT} # If you use StartTLS: 587
  username: ${SMTP_USERNAME}
  password: ${SMTP_PASSWORD}
  tls: ${SMTP_TLS} # If you use StartTLS: false
  disable_starttls: ${SMTP_DISABLE_STARTTLS}
  ca_file: null # Used for self signed certificates
  from_address: '${SERVEUR_HOST}'

email:
  body:
    signature: "${EMAIL_SIGNATURE}"
  subject:
    prefix: "${EMAIL_PREFIX}"

# From the project root directory
storage:
  tmp: '${PEERTUBE_DATAS}tmp/' # Use to download data (imports etc), store uploaded files before processing...
  avatars: '${PEERTUBE_DATAS}avatars/'
  videos: '${PEERTUBE_DATAS}videos/'
  streaming_playlists: '${PEERTUBE_DATAS}streaming-playlists/'
  redundancy: '${PEERTUBE_DATAS}redundancy/'
  logs: '${PEERTUBE_DATAS}logs/'
  previews: '${PEERTUBE_DATAS}previews/'
  thumbnails: '${PEERTUBE_DATAS}thumbnails/'
  torrents: '${PEERTUBE_DATAS}torrents/'
  captions: '${PEERTUBE_DATAS}captions/'
  cache: '${PEERTUBE_DATAS}cache/'
  plugins: '${PEERTUBE_DATAS}plugins/'

log:
  level: '${PEERTUBE_LOG_LEVEL}' # debug/info/warning/error
  rotation:
    enabled : true # Enabled by default, if disabled make sure that 'storage.logs' is pointing to a folder handled by logrotate

search:
  # Add ability to fetch remote videos/actors by their URI, that may not be federated with your instance
  # If enabled, the associated group will be able to "escape" from the instance follows
  # That means they will be able to follow channels, watch videos, list videos of non followed instances
  remote_uri:
    users: true
    anonymous: false

trending:
  videos:
    interval_days: 7 # Compute trending videos for the last x days

# Cache remote videos on your server, to help other instances to broadcast the video
# You can define multiple caches using different sizes/strategies
# Once you have defined your strategies, choose which instances you want to cache in admin -> manage follows -> following
redundancy:
  videos:
    check_interval: '1 hour' # How often you want to check new videos to cache
    strategies: # Just uncomment strategies you want
#      -
#        size: '10GB'
#        # Minimum time the video must remain in the cache. Only accept values > 10 hours (to not overload remote instances)
#        min_lifetime: '48 hours'
#        strategy: 'most-views' # Cache videos that have the most views
#      -
#        size: '10GB'
#        # Minimum time the video must remain in the cache. Only accept values > 10 hours (to not overload remote instances)
#        min_lifetime: '48 hours'
#        strategy: 'trending' # Cache trending videos
#      -
#        size: '10GB'
#        # Minimum time the video must remain in the cache. Only accept values > 10 hours (to not overload remote instances)
#        min_lifetime: '48 hours'
#        strategy: 'recently-added' # Cache recently added videos
#        min_views: 10 # Having at least x views

csp:
  enabled: false
  report_only: true # CSP directives are still being tested, so disable the report only mode at your own risk!
  report_uri:

tracker:
  # If you disable the tracker, you disable the P2P aspect of PeerTube
  enabled: true
  # Only handle requests on your videos.
  # If you set this to false it means you have a public tracker.
  # Then, it is possible that clients overload your instance with external torrents
  private: true
  # Reject peers that do a lot of announces (could improve privacy of TCP/UDP peers)
  reject_too_many_announces: false

history:
  videos:
    # If you want to limit users videos history
    # -1 means there is no limitations
    # Other values could be '6 months' or '30 days' etc (PeerTube will periodically delete old entries from database)
    max_age: ${PEERTUBE_VIDEO_HISTORY}

views:
  videos:
    # PeerTube creates a database entry every hour for each video to track views over a period of time
    # This is used in particular by the Trending page
    # PeerTube could remove old remote video views if you want to reduce your database size (video view counter will not be altered)
    # -1 means no cleanup
    # Other values could be '6 months' or '30 days' etc (PeerTube will periodically delete old entries from database)
    remote:
      max_age: ${PEERTUBE_VIEWS_HISTORY}

plugins:
  # The website PeerTube will ask for available PeerTube plugins and themes
  # This is an unmoderated plugin index, so only install plugins/themes you trust
  index:
    enabled: true
    check_latest_versions_interval: '12 hours' # How often you want to check new plugins/themes versions
    url: 'https://packages.joinpeertube.org'


###############################################################################
#
# From this point, all the following keys can be overridden by the web interface
# (local-production.json file). If you need to change some values, prefer to
# use the web interface because the configuration will be automatically
# reloaded without any need to restart PeerTube.
#
# /!\ If you already have a local-production.json file, the modification of the
# following keys will have no effect /!\.
#
###############################################################################

cache:
  previews:
    size: 500 # Max number of previews you want to cache
  captions:
    size: 500 # Max number of video captions/subtitles you want to cache

admin:
  # Used to generate the root user at first startup
  # And to receive emails from the contact form
  email: '${ROOT_USER_MAIL}'

contact_form:
  enabled: true

signup:
  enabled: false
  limit: 10 # When the limit is reached, registrations are disabled. -1 == unlimited
  requires_email_verification: false
  filters:
    cidr: # You can specify CIDR ranges to whitelist (empty = no filtering) or blacklist
      whitelist: []
      blacklist: []

user:
  # Default value of maximum video BYTES the user can upload (does not take into account transcoded files).
  # -1 == unlimited
  video_quota: -1
  video_quota_daily: -1

# If enabled, the video will be transcoded to mp4 (x264) with "faststart" flag
# In addition, if some resolutions are enabled the mp4 video file will be transcoded to these new resolutions.
# Please, do not disable transcoding since many uploaded videos will not work
transcoding:
  enabled: true
  # Allow your users to upload .mkv, .mov, .avi, .flv videos
  allow_additional_extensions: true
  # If a user uploads an audio file, PeerTube will create a video by merging the preview file and the audio file
  allow_audio_files: true
  threads: 1
  resolutions: # Only created if the original video has a higher resolution, uses more storage!
    0p: false # audio-only (creates mp4 without video stream, always created when enabled)
    240p: false
    360p: false
    480p: false
    720p: false
    1080p: false
    2160p: false

  # Generate videos in a WebTorrent format (what we do since the first PeerTube release)
  # If you also enabled the hls format, it will multiply videos storage by 2
  webtorrent:
    enabled: true

  # /!\ Requires ffmpeg >= 4.1
  # Generate HLS playlists and fragmented MP4 files. Better playback than with WebTorrent:
  #     * Resolution change is smoother
  #     * Faster playback in particular with long videos
  #     * More stable playback (less bugs/infinite loading)
  # If you also enabled the webtorrent format, it will multiply videos storage by 2
  hls:
    enabled: false

import:
  # Add ability for your users to import remote videos (from YouTube, torrent...)
  videos:
    http: # Classic HTTP or all sites supported by youtube-dl https://rg3.github.io/youtube-dl/supportedsites.html
      enabled: false
    torrent: # Magnet URI or torrent file (use classic TCP/UDP/WebSeed to download the file)
      enabled: false

auto_blacklist:
  # New videos automatically blacklisted so moderators can review before publishing
  videos:
    of_users:
      enabled: false

# Instance settings
instance:
  name: 'PeerTube'
  short_description: 'PeerTube, a federated (ActivityPub) video streaming platform using P2P (BitTorrent) directly in the web browser with WebTorrent and Angular.'
  description: 'Welcome to this PeerTube instance!' # Support markdown
  terms: 'No terms for now.' # Support markdown
  code_of_conduct: '' # Supports markdown

  # Who moderates the instance? What is the policy regarding NSFW videos? Political videos? etc
  moderation_information: '' # Supports markdown

  # Why did you create this instance?
  creation_reason: ''

  # Who is behind the instance? A single person? A non profit?
  administrator: ''

  # How long do you plan to maintain this instance?
  maintenance_lifetime: ''

  # How will you pay the PeerTube instance server? With you own funds? With users donations? Advertising?
  business_model: ''

  # If you want to explain on what type of hardware your PeerTube instance runs
  # Example: "2 vCore, 2GB RAM..."
  hardware_information: '${SERVEUR_HARDWARE}' # Supports Markdown

  # What are the main languages of your instance? To interact with your users for example
  # Uncomment or add the languages you want
  # List of supported languages: https://peertube.cpy.re/api/v1/videos/languages
  languages:
#    - en
#    - es
#    - fr

  # You can specify the main categories of your instance (dedicated to music, gaming or politics etc)
  # Uncomment or add the category ids you want
  # List of supported categories: https://peertube.cpy.re/api/v1/videos/categories
  categories:
#    - 1  # Music
#    - 2  # Films
#    - 3  # Vehicles
#    - 4  # Art
#    - 5  # Sports
#    - 6  # Travels
#    - 7  # Gaming
#    - 8  # People
#    - 9  # Comedy
#    - 10 # Entertainment
#    - 11 # News & Politics
#    - 12 # How To
#    - 13 # Education
#    - 14 # Activism
#    - 15 # Science & Technology
#    - 16 # Animals
#    - 17 # Kids
#    - 18 # Food

  default_client_route: '/videos/trending'

  # Whether or not the instance is dedicated to NSFW content
  # Enabling it will allow other administrators to know that you are mainly federating sensitive content
  # Moreover, the NSFW checkbox on video upload will be automatically checked by default
  is_nsfw: false
  # By default, "do_not_list" or "blur" or "display" NSFW videos
  # Could be overridden per user with a setting
  default_nsfw_policy: 'do_not_list'

  customizations:
    javascript: '' # Directly your JavaScript code (without <script> tags). Will be eval at runtime
    css: '' # Directly your CSS code (without <style> tags). Will be injected at runtime
  # Robot.txt rules. To disallow robots to crawl your instance and disallow indexation of your site, add '/' to "Disallow:'
  robots: |
    User-agent: *
    Disallow:
  # Security.txt rules. To discourage researchers from testing your instance and disable security.txt integration, set this to an empty string.
  securitytxt:
    "# If you would like to report a security issue\n# you may report it to:\nContact: https://github.com/Chocobozzz/PeerTube/blob/develop/SECURITY.md\nContact: mailto:"

services:
  # Cards configuration to format video in Twitter
  twitter:
    username: '${TWITTER}' # Indicates the Twitter account for the website or platform on which the content was published
    # If true, a video player will be embedded in the Twitter feed on PeerTube video share
    # If false, we use an image link card that will redirect on your PeerTube instance
    # Change it to "true", and then test on https://cards-dev.twitter.com/validator to see if you are whitelisted
    whitelisted: true

followers:
  instance:
    # Allow or not other instances to follow yours
    enabled: true
    # Whether or not an administrator must manually validate a new follower
    manual_approval: false

followings:
  instance:
    # If you want to automatically follow instances of the public index
    # If this option is enabled, use the mute feature instead of deleting followings
    # /!\ Don't enable this if you don't have a reactive moderation team /!\
    auto_follow_index:
      enabled: ${PEERTUBE_AUTO_FOLLOW}
      index_url: 'https://instances.joinpeertube.org'
    auto_follow_back:
      enabled: false

theme:
  default: 'default'
EOF

## INSTALL CERTBOT
echo "$(tput bold)[$(date -u +"%Y-%m-%dT%H:%M:%SZ")]$(tput setaf 2)[INFO]$(tput sgr0) Install/Config Dehydrated certbot ...$(tput sgr0)"

curl "${DEHYDRATED_URL}" -o "${DEHYDRATED_BIN}"
chmod +x "${DEHYDRATED_BIN}" || err "Fail tu exec ${DEHYDRATED_BIN}"

mkdir -p "${DEHYDRATED_CONFIG_FOLDER}/certs"
mkdir -p "${DEHYDRATED_CONFIG_FOLDER}/chains"
mkdir -p "${DEHYDRATED_CONFIG_FOLDER}/accounts"

if [ -n "${SERVEUR_ALIAS}" ]; then
    echo "${SERVEUR_HOST} ${SERVEUR_ALIAS}" > "${DEHYDRATED_CONFIG_FOLDER}/domains"
else
    echo "${SERVEUR_HOST}" > "${DEHYDRATED_CONFIG_FOLDER}/domains"
fi

cat <<EOF >"${DEHYDRATED_CONFIG_FOLDER}/conf"
KEY_ALGO=rsa
WELLKNOWN="${HTTP_WELLKNOW_FOLDER}"
CHALLENGETYPE="http-01"
CONTACT_EMAIL="${ROOT_USER_MAIL}"
CERTDIR="${DEHYDRATED_CONFIG_FOLDER}/certs"
LOCKFILE="${DEHYDRATED_CONFIG_FOLDER}/lock"
ACCOUNTDIR="${DEHYDRATED_CONFIG_FOLDER}/accounts"
CHAINCACHE="${DEHYDRATED_CONFIG_FOLDER}/chains"
DOMAINS_TXT="${DEHYDRATED_CONFIG_FOLDER}/domains"
ALPNCERTDIR="${DEHYDRATED_CONFIG_FOLDER}/alpn-certs"
EOF

(crontab -l 2>/dev/null; echo "@weekly ${DEHYDRATED_BIN} -c --config ${DEHYDRATED_CONFIG_FOLDER}/conf") | crontab -

## GEN CERTIFICAT
echo "$(tput bold)[$(date -u +"%Y-%m-%dT%H:%M:%SZ")]$(tput setaf 2)[INFO]$(tput sgr0) Add vhost:80 for certbot ...$(tput sgr0)"

rm "${NGINX_SITE_ENABLE}*"

cat <<EOF >"${NGINX_SITE_AVAILABLE}cerbot"
server{
    listen 80;
    server_name ${SERVEUR_HOST} ${SERVEUR_ALIAS};

    location /.well-known/acme-challenge {
        alias ${HTTP_WELLKNOW_FOLDER};
    }

    location ~ ^\/(?!.well-known).* {
        return 301 https://\$host\$request_uri;
    }
}
EOF

ln -s "${NGINX_SITE_AVAILABLE}cerbot" "${NGINX_SITE_ENABLE}"

mkdir -p "${HTTP_WELLKNOW_FOLDER}"

systemctl enable nginx
systemctl restart nginx || err "Fail to start NGINX"

echo "$(tput bold)[$(date -u +"%Y-%m-%dT%H:%M:%SZ")]$(tput setaf 2)[INFO]$(tput sgr0) Generate certificate ...$(tput sgr0)"

${DEHYDRATED_BIN} --register --accept-terms --config "${DEHYDRATED_CONFIG_FOLDER}/conf" || err "Dehydrated fail to accept-terms"
${DEHYDRATED_BIN} -c --config "${DEHYDRATED_CONFIG_FOLDER}/conf" || err "Probléme avec la génération du certif lets encrypt"

## CONFIG NGINX
echo "$(tput bold)[$(date -u +"%Y-%m-%dT%H:%M:%SZ")]$(tput setaf 2)[INFO]$(tput sgr0) Add vhost:443 on proxy for Peertube ...$(tput sgr0)"

cat <<EOF >"${NGINX_SITE_AVAILABLE}peertube"
server {
  listen 443 ssl http2;
  listen [::]:443 ssl http2;
  server_name ${SERVEUR_HOST} ${SERVEUR_ALIAS};

  if (\$host != \$server_name) {
    rewrite ^/(.*) \$scheme://\$server_name/\$1 permanent;
  }

  # For example with certbot (you need a certificate to run https)
  ssl_certificate      ${DEHYDRATED_CONFIG_FOLDER}/certs/${SERVEUR_HOST}/fullchain.pem;
  ssl_certificate_key  ${DEHYDRATED_CONFIG_FOLDER}/certs/${SERVEUR_HOST}/privkey.pem;

  # Security hardening (as of 11/02/2018)
  ssl_protocols TLSv1.2; # TLSv1.3, TLSv1.2 if nginx >= 1.13.0
  ssl_prefer_server_ciphers on;
  ssl_ciphers 'ECDHE-ECDSA-AES256-GCM-SHA384:ECDHE-RSA-AES256-GCM-SHA384:ECDHE-ECDSA-CHACHA20-POLY1305:ECDHE-RSA-CHACHA20-POLY1305:ECDHE-ECDSA-AES128-GCM-SHA256:ECDHE-RSA-AES128-GCM-SHA256:ECDHE-ECDSA-AES256-SHA384:ECDHE-RSA-AES256-SHA384:ECDHE-ECDSA-AES128-SHA256:ECDHE-RSA-AES128-SHA256';
  # ssl_ecdh_curve secp384r1; # Requires nginx >= 1.1.0, not compatible with import-videos script
  ssl_session_timeout  10m;
  ssl_session_cache shared:SSL:10m;
  ssl_session_tickets off; # Requires nginx >= 1.5.9
  ssl_stapling on; # Requires nginx >= 1.3.7
  ssl_stapling_verify on; # Requires nginx => 1.3.7

  # Configure with your resolvers
  # resolver \$DNS-IP-1 \$DNS-IP-2 valid=300s;
  # resolver_timeout 5s;

  # Enable compression for JS/CSS/HTML bundle, for improved client load times.
  # It might be nice to compress JSON, but leaving that out to protect against potential
  # compression+encryption information leak attacks like BREACH.
  gzip on;
  gzip_types text/css application/javascript;
  gzip_vary on;

  # If you have a small /var/lib partition, it could be interesting to store temp nginx uploads in a different place
  # See https://nginx.org/en/docs/http/ngx_http_core_module.html#client_body_temp_path
  # client_body_temp_path /var/www/peertube/storage/nginx/;

  # Enable HSTS
  # Tells browsers to stick with HTTPS and never visit the insecure HTTP
  # version. Once a browser sees this header, it will only visit the site over
  # HTTPS for the next 2 years: (read more on hstspreload.org)
  #add_header Strict-Transport-Security "max-age=63072000; includeSubDomains";

  access_log ${HTTP_PEERTUBE_LOG}${SERVEUR_HOST}.access.log;
  error_log  ${HTTP_PEERTUBE_LOG}${SERVEUR_HOST}.error.log;

  location ^~ '/.well-known/acme-challenge' {
    default_type "text/plain";
    root ${HTTP_WELLKNOW_FOLDER};
  }

  # Bypass PeerTube for performance reasons. Could be removed
  location ~ ^/client/(.*\.(js|css|png|svg|woff2|otf|ttf|woff|eot))\$ {
    add_header Cache-Control "public, max-age=31536000, immutable";

    alias ${PEERTUBE_FOLDER}peertube-latest/client/dist/\$1;
  }

  # Bypass PeerTube for performance reasons. Could be removed
  location ~ ^/static/(thumbnails|avatars)/ {
    if (\$request_method = 'OPTIONS') {
      add_header 'Access-Control-Allow-Origin' '*';
      add_header 'Access-Control-Allow-Methods' 'GET, OPTIONS';
      add_header 'Access-Control-Allow-Headers' 'Range,DNT,X-CustomHeader,Keep-Alive,User-Agent,X-Requested-With,If-Modified-Since,Cache-Control,Content-Type';
      add_header 'Access-Control-Max-Age' 1728000;
      add_header 'Content-Type' 'text/plain charset=UTF-8';
      add_header 'Content-Length' 0;
      return 204;
    }

    add_header 'Access-Control-Allow-Origin' '*';
    add_header 'Access-Control-Allow-Methods' 'GET, OPTIONS';
    add_header 'Access-Control-Allow-Headers' 'Range,DNT,X-CustomHeader,Keep-Alive,User-Agent,X-Requested-With,If-Modified-Since,Cache-Control,Content-Type';

    # Cache 2 hours
    add_header Cache-Control "public, max-age=7200";

    root ${PEERTUBE_DATAS};

    rewrite ^/static/(thumbnails|avatars)/(.*)\$ /\$1/\$2 break;
    try_files \$uri /;
  }

  location / {
    proxy_pass http://127.0.0.1:${PEERTUBE_PORT};
    proxy_set_header X-Real-IP \$remote_addr;
    proxy_set_header Host \$host;
    proxy_set_header X-Forwarded-For \$proxy_add_x_forwarded_for;

    # This is the maximum upload size, which roughly matches the maximum size of a video file
    # you can send via the API or the web interface. By default this is 8GB, but administrators
    # can increase or decrease the limit. Currently there's no way to communicate this limit
    # to users automatically, so you may want to leave a note in your instance 'about' page if
    # you change this.
    #
    # Note that temporary space is needed equal to the total size of all concurrent uploads.
    # This data gets stored in /var/lib/nginx by default, so you may want to put this directory
    # on a dedicated filesystem.
    #
    client_max_body_size 8G;

    proxy_connect_timeout       600;
    proxy_send_timeout          600;
    proxy_read_timeout          600;
    send_timeout                600;
  }

  # Bypass PeerTube for performance reasons. Could be removed
  location ~ ^/static/(webseed|redundancy|streaming-playlists)/ {
    # Clients usually have 4 simultaneous webseed connections, so the real limit is 3MB/s per client
    set \$peertube_limit_rate 800k;

    # Increase rate limit in HLS mode, because we don't have multiple simultaneous connections
    if (\$request_uri ~ -fragmented.mp4\$) {
      set \$peertube_limit_rate 5000k;
    }

    # Use this with nginx >= 1.17.0
    # limit_rate \$peertube_limit_rate;
    # Or this if your nginx < 1.17.0
    set \$limit_rate \$peertube_limit_rate;
    limit_rate_after 5000k;

    if (\$request_method = 'OPTIONS') {
      add_header 'Access-Control-Allow-Origin' '*';
      add_header 'Access-Control-Allow-Methods' 'GET, OPTIONS';
      add_header 'Access-Control-Allow-Headers' 'Range,DNT,X-CustomHeader,Keep-Alive,User-Agent,X-Requested-With,If-Modified-Since,Cache-Control,Content-Type';
      add_header 'Access-Control-Max-Age' 1728000;
      add_header 'Content-Type' 'text/plain charset=UTF-8';
      add_header 'Content-Length' 0;
      return 204;
    }

    if (\$request_method = 'GET') {
      add_header 'Access-Control-Allow-Origin' '*';
      add_header 'Access-Control-Allow-Methods' 'GET, OPTIONS';
      add_header 'Access-Control-Allow-Headers' 'Range,DNT,X-CustomHeader,Keep-Alive,User-Agent,X-Requested-With,If-Modified-Since,Cache-Control,Content-Type';

      # Don't spam access log file with byte range requests
      access_log off;
    }

    root ${PEERTUBE_DATAS};

    rewrite ^/static/webseed/(.*)\$ /videos/\$1 break;
    rewrite ^/static/redundancy/(.*)\$ /redundancy/\$1 break;
    rewrite ^/static/streaming-playlists/(.*)\$ /streaming-playlists/\$1 break;

    try_files \$uri /;
  }

  # Websocket tracker
  location /tracker/socket {
    # Peers send a message to the tracker every 15 minutes
    # Don't close the websocket before this time
    proxy_read_timeout 1200s;
    proxy_set_header Upgrade \$http_upgrade;
    proxy_set_header Connection "upgrade";
    proxy_http_version 1.1;
    proxy_set_header X-Forwarded-For \$proxy_add_x_forwarded_for;
    proxy_set_header Host \$host;
    proxy_pass http://127.0.0.1:${PEERTUBE_PORT};
  }

  location /socket.io {
    proxy_set_header X-Forwarded-For \$proxy_add_x_forwarded_for;
    proxy_set_header Host \$host;

    proxy_pass http://127.0.0.1:${PEERTUBE_PORT};

    # enable WebSockets
    proxy_http_version 1.1;
    proxy_set_header Upgrade \$http_upgrade;
    proxy_set_header Connection "upgrade";
  }
}
EOF

ln -s "${NGINX_SITE_AVAILABLE}peertube" "${NGINX_SITE_ENABLE}"

systemctl restart nginx || err "Fail to restart NGINX"

## SYSTEMD SERVICE
echo "$(tput bold)[$(date -u +"%Y-%m-%dT%H:%M:%SZ")]$(tput setaf 2)[INFO]$(tput sgr0) Create and enable Peertube on systemd ...$(tput sgr0)"

cat <<EOF >"/etc/systemd/system/${PEERTUBE_SERVICE}"
[Unit]
Description=PeerTube daemon
After=network.target postgresql.service redis-server.service

[Service]
Type=simple
Environment=NODE_ENV=production
Environment=NODE_CONFIG_DIR=${PEERTUBE_FOLDER}config
User=${PEERTUBE_USER}
Group=${PEERTUBE_USER}
ExecStart=/usr/bin/npm start
WorkingDirectory=${PEERTUBE_FOLDER}peertube-latest
StandardOutput=syslog
StandardError=syslog
SyslogIdentifier=peertube
Restart=always

; Some security directives.
; Mount /usr, /boot, and /etc as read-only for processes invoked by this service.
ProtectSystem=full
; Sets up a new /dev mount for the process and only adds API pseudo devices
; like /dev/null, /dev/zero or /dev/random but not physical devices. Disabled
; by default because it may not work on devices like the Raspberry Pi.
PrivateDevices=false
; Ensures that the service process and all its children can never gain new
; privileges through execve().
NoNewPrivileges=true
; This makes /home, /root, and /run/user inaccessible and empty for processes invoked
; by this unit. Make sure that you do not depend on data inside these folders.
ProtectHome=true
; Drops the sys admin capability from the daemon.
CapabilityBoundingSet=~CAP_SYS_ADMIN

[Install]
WantedBy=multi-user.target
EOF

systemctl daemon-reload
systemctl enable ${PEERTUBE_SERVICE}
systemctl start ${PEERTUBE_SERVICE} || err "Fail to start peertube deamon"

## SSH
echo "$(tput bold)[$(date -u +"%Y-%m-%dT%H:%M:%SZ")]$(tput setaf 2)[INFO]$(tput sgr0) Change SSH config file ...$(tput sgr0)"

cp /etc/ssh/sshd_config /etc/ssh/sshd_config.back
sed -rie "s/#?Port [0-9]+/Port ${SSH_PORT}/g" "${SSH_CONFIG_FILE}"
sed -rie "s/#?PasswordAuthentication yes/PasswordAuthentication ${SSH_PASSWD_AUTH}/g" "${SSH_CONFIG_FILE}"
echo "${SSH_PUB_KEY}" >> "${SSH_AUTHORIZED_KEY_FILE}"

systemctl restart sshd || err "Fail to restart sshd deamon" "Keep peertube"

## FIREWALL
UFW_RULES="ufw allow 'WWW Full' && ufw allow ${SSH_PORT}/tcp && ufw limit ${SSH_PORT}/tcp"
echo "$(tput bold)[$(date -u +"%Y-%m-%dT%H:%M:%SZ")]$(tput setaf 2)[INFO]$(tput sgr0) Config/Enable ufw firewall ...$(tput sgr0)"

if [[ ${FIREWALL_TYPE} == "cloud" ]]; then
    yes | ufw disable
    echo "$(tput bold)[$(date -u +"%Y-%m-%dT%H:%M:%SZ")]$(tput setaf 3)[WARN]$(tput sgr0) On firewall Cloud Platform interface open : 80 443 ${SSH_PORT} And allow SMTP $(tput sgr0)"
elif [[ ${FIREWALL_TYPE} == "baremetal" ]]; then
    sed -rie 's/#?DEFAULT_INPUT_POLICY="\w+"/DEFAULT_INPUT_POLICY="ACCEPT"/g' "/etc/default/ufw"
    sed -rie 's/^COMMIT/-A ufw-reject-input -j DROP\nCOMMIT/g' "/etc/ufw/after.rules"

    ufw logging off
    ${UFW_RULES}
    yes | ufw enable || err "Fail to enable UFW" "Keep peertube"
else
    yes | ufw reset
    ufw logging low
    ufw default deny incoming
    ufw default allow outgoing
    ${UFW_RULES}
    yes | ufw enable || err "Fail to enable UFW" "Keep peertube"
fi

## SHOW RESULT DATAS
echo "$(tput bold)[$(date -u +"%Y-%m-%dT%H:%M:%SZ")]$(tput setaf 2)[INFO]$(tput sgr0) Peertube server is on the road ! $(tput sgr0)"

echo "
$(tput bold)--- --- INFOS --- ---$(tput sgr0)
$(tput bold)INSTALL LOG$(tput sgr0) '${INSTALL_SCRIPT_LOGFILE}'

$(tput bold)Public$(tput sgr0) : ${SERVEUR_HOST}
$(tput bold)Local $(tput sgr0) : http://127.0.0.1:${PORT}

$(tput bold)Dossier$(tput sgr0) : ${PEERTUBE_FOLDER}
$(tput bold)Config $(tput sgr0) : ${PEERTUBE_CONFIG_FILE}
$(tput bold)Datas  $(tput sgr0) : ${PEERTUBE_DATAS}
$(tput bold)SystemD$(tput sgr0) : /etc/systemd/system/${PEERTUBE_SERVICE}

$(tput bold)UserNameServer$(tput sgr0) : ${PEERTUBE_USER}
$(tput bold)UserPasswdServer$(tput sgr0) : <null>

$(tput bold)UserNamedb$(tput sgr0) : ${PEERTUBE_USER}
$(tput bold)UserPasswdDB$(tput sgr0) : ${PEERTUBE_DB_PASSWORD}

$(tput bold)RootUserHTTP$(tput sgr0) : ${PEERTUBE_HTTP_USER}
$(tput bold)RootPassHTTP$(tput sgr0) : ${PT_INITIAL_ROOT_PASSWORD}
"

exit 0
